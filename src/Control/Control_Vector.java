/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Control;

import Modelo.EnteroGrande;
import Vista.Consola2;

/**
 *
 * @author Lab05pcdocente
 */
public class Control_Vector {
    
    public static void main(String[] args) {
        //Vista:
        Consola2 myConsola = new Consola2();
        String cadena = myConsola.leerCadena("Digite una cadena:");

        //Model:
        EnteroGrande entero = new EnteroGrande(cadena);
        
        myConsola.imprimir("El vector es:" + entero.toString());
        
        entero.sort();
        
        myConsola.imprimir("El vector es:" + entero.toString());
        
    }
}
